<?php

namespace app\components\redirector\interfaces;

/**
 * Interface ParserInterface
 */
interface ParserInterface
{

    /**
     * @param string $url_with_404
     * @param string $source
     * @return string
     */
    public static function findChangedUrlInSource(string $url_with_404, string $source): string;
}