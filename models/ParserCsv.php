<?php

namespace app\components\redirector\models;

use Yii;
use app\components\redirector\interfaces\ParserInterface;

/**
 * 
 */
class ParserCsv extends \yii\base\Model implements ParserInterface
{
    
    public $source;
    /**
     * Функция ищет соответствие ошибочному адресу $not_found_url в csv-файле подстановки
     * $source. В случае успеха возвращает валидный адрес, в противном случае - пустую строку.
     * @param string $not_found_url
     * @param string $source
     * @return string
     */
    public static function findChangedUrlInSource(string $url_with_404, string $source): string
    {
        if (($handle = fopen(Yii::getAlias('@app') . $source, 'r')) !== FALSE) {            
            while (($redirection_pattern = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if ($redirection_pattern[0] == $url_with_404) {
                    return $redirection_pattern[1];
                }
            }
            fclose($handle);
        }        
        return '';
    }            
} 