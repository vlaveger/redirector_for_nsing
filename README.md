В конфиге html/framework/config/frontend.php прописать:
    
    1. $config = [
        ... 
          'on beforeAction' => ['app\components\redirector\Redirector','redirect'],
        ...
    
    2.  'container' => [
            'definitions' => [
            ...
                app\components\redirector\interfaces::class => [
                    'class' => 'app\components\redirector\models\ParserCsv',
                    'source' => '/components/redirector/files/redirect.csv'
                ],
            ...

    3. 'components' => [
        ...
            'redirector' => [
                'class' => 'app\components\redirector\Redirector',
            ],
        ...
    