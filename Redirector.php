<?php

namespace app\components\redirector;

use Yii;
use yii\base\Component;
use app\components\redirector\models\ParserCsv;

class Redirector extends Component 
{
    public $source;
          
    public static function redirect()
    {
        if (Yii::$app->errorHandler->exception && Yii::$app->errorHandler->exception->statusCode == '404'){
            $parser = \YII::$container->get('app\components\redirector\interfaces');
            $redirect_url = $parser->findChangedUrlInSource(Yii::$app->request->url, $parser->source);
            if ($redirect_url) {
                return Yii::$app->getResponse()->redirect($redirect_url, 301)->send();
            }
        }
    }
}
